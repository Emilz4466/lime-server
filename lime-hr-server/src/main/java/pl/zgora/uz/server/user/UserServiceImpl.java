package pl.zgora.uz.server.user;

import org.springframework.stereotype.Service;

import pl.zgora.uz.interfacesabstracts.ServiceAbstract;
import pl.zgora.uz.pub.user.UserService;

@Service
public class UserServiceImpl extends ServiceAbstract<UserDao, UserEntity> implements UserService {

}

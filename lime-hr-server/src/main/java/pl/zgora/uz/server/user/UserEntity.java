package pl.zgora.uz.server.user;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import pl.zgora.uz.interfacesabstracts.EntityInterface;

@Entity
public class UserEntity extends EntityInterface {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long idUser;

	private String firstName;
	private String secondName;
	private String surname;

	public Long getId() {
		return idUser;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getSecondName() {
		return secondName;
	}

	public String getSurname() {
		return surname;
	}

}

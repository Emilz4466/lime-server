package pl.zgora.uz.pub.user;

import pl.zgora.uz.interfacesabstracts.EntityInterface;
import pl.zgora.uz.interfacesabstracts.ServiceInterface;

public interface UserService extends ServiceInterface<EntityInterface> {

}

package pl.zgora.uz;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import pl.zgora.uz.pub.user.UserService;
import pl.zgora.uz.server.user.UserEntity;

@Controller
public class ControllerImpl {

	@Autowired
	UserService userService;

	@RequestMapping("/user/{id}/")
	public ResponseEntity<UserEntity> getUserById(@PathVariable("id") long id) {
		UserEntity userEntity = (UserEntity) userService.getById(id);
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		return new ResponseEntity<UserEntity>(userEntity, headers, HttpStatus.OK);
	}

}

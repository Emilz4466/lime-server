package pl.zgora.uz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties
public class LimeHrServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(LimeHrServerApplication.class, args);
	}

}

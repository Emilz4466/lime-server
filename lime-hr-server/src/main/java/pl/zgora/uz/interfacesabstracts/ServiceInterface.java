package pl.zgora.uz.interfacesabstracts;

@SuppressWarnings("hiding")
public interface ServiceInterface<E extends EntityInterface> {

	E save(E entity);

	E getById(long id);

	void delete(long id);
}

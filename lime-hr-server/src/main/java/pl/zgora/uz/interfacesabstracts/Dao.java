package pl.zgora.uz.interfacesabstracts;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

@SuppressWarnings("hiding")
@Repository
public abstract class Dao<E extends EntityInterface> {

	Map<Long, E> allEnitites = new HashMap<>();

	EntityInterface save(EntityInterface entity) {
		allEnitites.put(entity.getId(), (E) entity);
		return entity;
	}

	List<EntityInterface> findAll() {
		List<EntityInterface> entities = new ArrayList(allEnitites.values());
		return entities;
	}

	EntityInterface find(long id) {
		return allEnitites.get(id);
	}

	void delete(long id) {
		allEnitites.remove(id);
	}

}

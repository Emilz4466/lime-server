package pl.zgora.uz.interfacesabstracts;

import org.springframework.beans.factory.annotation.Autowired;

public abstract class ServiceAbstract<D extends Dao, E extends EntityInterface> {

	@Autowired
	D dao;

	public EntityInterface save(EntityInterface entity) {
		E e = (E) entity;
		return dao.save(e);
	}

	public E getById(long id) {
		return (E) dao.find(id);
	}

	public void delete(long id) {
		dao.delete(id);
	}
}
